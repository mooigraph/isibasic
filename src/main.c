
/*
 *  Copyright t lefering
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */

#include <stdio.h>
#include <stdlib.h>

#include "main.h"
#include "isi.h"
#include "parse.h"

static struct isi_graph *maingraph = NULL;

static FILE *ifile = NULL;

int
main (int argc, char *argv[])
{
  struct isi_nlist *n;
  struct isi_elist *e;

  if (argc != 2)
    {
      printf ("usage: isibasic datafile.txt\n");
      return (0);
    }

  ifile = fopen ( argv[1] /* "example.txt" */ , "r");

  if (ifile == NULL)
    {
      printf ("cannot open file %s\n", argv[1]);
      return (0);
    }

  maingraph = calloc (1,sizeof (struct isi_graph));

  /* offset of drawing */
  maingraph->xbase = 1;
  maingraph->ybase = 1;

  /* (x,y) spacing of nodes */
  maingraph->xspace = 5;
  maingraph->yspace = 5;

  parse (ifile, maingraph);

  fclose (ifile);

  /* check if parsed node/edge data oke */
  if (maingraph->nnodes == 0 || maingraph->nedges == 0)
    {
      printf ("no nodes or edges in graph data\n");
      return (0);
    }

  /* run the layout */
  isilayout (maingraph);

  /* print result */
  n = maingraph->headnode;

  while (n)
    {
      printf ("node %d indegree=%d outdegree=%d is at (%d,%d)\n",
	      n->node->number, n->node->countpred, n->node->countsucc,
	      n->node->x, n->node->y);
      n = n->next;
    }

  e = maingraph->headedge;

  while (e)
    {
      printf ("edge %d->%d\n", e->edge->fnode, e->edge->tnode);
      e = e->next;
    }

  return (0);
}

/* end */
