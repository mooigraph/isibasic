
/*
 *  Copyright t lefering
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "main.h"
#include "parse.h"

static struct isi_node *findnode (struct isi_graph *g, int number);

void
parse (FILE * ifile, struct isi_graph *g)
{
  char buf[256];
  int i;
  int c;
  int fn;
  int tn;
  int status;
  struct isi_edge *e;
  struct isi_elist *el;
  struct isi_node *n;
  struct isi_nlist *nl;
  struct isi_node *fns;
  struct isi_node *tns;

  g->maxnn = 0;
  g->nnodes = 0;
  g->nedges = 0;

  for (;;)
    {

      /* read a line */
      memset (buf, 0, 256);

      for (i = 0; i < 256; i++)
	{
	  c = fgetc (ifile);
	  if (c == EOF || c == '\n')
	    {
	      break;
	    }
	  buf[i] = c;
	}

      if (c == EOF || buf[0] == 0)
	{
	  break;
	}

      /* check line */
      if (buf[0] == '#')
	{
	  /* skip comment lines */
	  continue;
	}

      /* assume edge line as in "2,3" */
      status = sscanf (buf, "%d,%d", &fn, &tn);

      if (status != 2)
	{
	  /* error in parsed string */
	  break;
	}

      /* check max node number */
      if (fn > g->maxnn)
	{
	  g->maxnn = fn;
	}

      if (tn > g->maxnn)
	{
	  g->maxnn = tn;
	}

      e = calloc (1, sizeof (struct isi_edge));

      e->fnode = fn;
      e->tnode = tn;

      el = calloc (1, sizeof (struct isi_elist));

      el->edge = e;

      if (g->headedge == NULL)
	{
	  g->headedge = el;
	}
      else
	{
	  g->tailedge->next = el;
	}

      g->tailedge = el;

      g->nedges = g->nedges + 1;

      /* stop at end of file */
      if (c == EOF)
	{
	  break;
	}

    }

  g->nnodes = g->maxnn + 1;


  /* check for parse error */
  if (status == EOF)
    {
      g->nnodes = 0;
      g->nedges = 0;
      return;
    }

  /* add the nodes */
  for (i = 0; i < g->nnodes; i++)
    {
      n = calloc (1, sizeof (struct isi_node));

      n->number = i;
      n->width = 15;
      n->height = 10;

      nl = calloc (1, sizeof (struct isi_nlist));

      nl->node = n;

      if (g->headnode == NULL)
	{
	  g->headnode = nl;
	}
      else
	{
	  g->tailnode->next = nl;
	}

      g->tailnode = nl;
    }

  /* configure pred and succlists in the nodes */
  el = g->headedge;

  while (el)
    {
      fn = el->edge->fnode;
      tn = el->edge->tnode;

      fns = findnode (g, fn);
      tns = findnode (g, tn);

      /* add outgoing edge */
      nl = calloc (1, sizeof (struct isi_nlist));
      nl->node = tns;

      if (fns->succlist == NULL)
	{
	  fns->succlist = nl;
	}
      else
	{
	  fns->succlisttail->next = nl;
	}
      fns->succlisttail = nl;

      /* number of outgoing edges of node */
      fns->countsucc = fns->countsucc + 1;

      /* add incoming edge */
      nl = calloc (1, sizeof (struct isi_nlist));
      nl->node = fns;

      if (tns->predlist == NULL)
	{
	  tns->predlist = nl;
	}
      else
	{
	  tns->predlisttail->next = nl;
	}
      tns->predlisttail = nl;

      /* number of incoming edges */
      tns->countpred = tns->countpred + 1;


      el = el->next;
    }

  return;
}

/* get node struct of node number */
static struct isi_node *
findnode (struct isi_graph *g, int number)
{
  struct isi_nlist *p;
  p = g->headnode;
  while (p)
    {

      if (p->node->number == number)
	{
	  return (p->node);
	}
      p = p->next;
    }
  /* shouldnothappen */
  return NULL;
}

/* end. */
