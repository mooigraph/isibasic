
/*
 *  Copyright t lefering
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */

#ifndef MAIN_H
#define MAIN_H 1

struct isi_graph;
struct isi_nlist;
struct isi_elist;
struct isi_node;
struct isi_edge;

struct isi_graph
{

	/* list of nodes */
	struct isi_nlist *headnode;
	struct isi_nlist *tailnode;

	/* list of edges */
	struct isi_elist *headedge;
	struct isi_elist *tailedge;

	int maxnn;	/* max number of node parsed */
	int nnodes;	/* number of nodes in graph */
	int nedges;	/* number of edges in graph */

	int xbase;	/* x offset of drawing */
	int ybase;	/* y offset of drawing */

	int xspace;	/* x spacing of nodes */
	int yspace;	/* y spacing of nodes */
};

/* a list of nodes */
struct isi_nlist
{
	struct isi_node *node;
	struct isi_nlist *next;
};

struct isi_node
{
	int number;	/* uniq number */
	int x;		/* rel. x position */
	int y;		/* rel. y position */
	int width;	/* x size */
	int height;	/* y size */
	int displayed;	/* set if on display */
	int countpred;	/* number of incoming edges */
	int countsucc;	/* number of outgoing edges */
	struct isi_nlist *succlist; /* outgoing edges nodes list */
	struct isi_nlist *succlisttail; /* outgoing edges nodes list */
	struct isi_nlist *predlist; /* incoming edges nodes list */
	struct isi_nlist *predlisttail; /* incoming edges nodes list */
};

struct isi_elist
{
	struct isi_edge *edge;
	struct isi_elist *next;
};

struct isi_edge
{
	int fnode;	/* from-node nunber */
	int tnode;	/* to-node number */
};

#endif

/* end */
